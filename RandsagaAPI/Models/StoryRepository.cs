﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Web;

namespace RandsagaAPI.Models
{
    public class StoryRepository : IStoryRepository
    {
        private List<Story> stories = new List<Story>();
        private int _nextId = 1;

        public StoryRepository(/*string name, string category, string author*/)
        {
            Add(new Story { Name = "teststory", Author = "testauthor", Category = "testcategory"});
        }

        public Story Get(int id)
        {
            return stories.Find(p => p.Id == id);
        }

        public Story Add(Story story)
        {
            if (story == null)
            {
                throw new ArgumentNullException("no story");
            }
            story.Id = _nextId++;
            stories.Add(story);
            return story;
        }

        public void Remove(int id)
        {
            stories.RemoveAll(p => p.Id == id);
        }

        public IEnumerable<Story> GetAll()
        {
            return stories;
        } 

    }
}