﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandsagaAPI.Models
{
    interface IStoryRepository
    {
        Story Get(int id);
        Story Add(Story item);
        void Remove(int id);
        IEnumerable<Story> GetAll();
    }
}
