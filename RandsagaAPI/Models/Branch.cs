﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace RandsagaAPI.Models
{
    public class Branch
    {
        public string title { get; set; }
        public string body { get; set; }
        public int branchId { get; set; }
        public int layer { get; set; }
        public Story story { get; set; }

    }
}