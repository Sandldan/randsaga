﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;

namespace RandsagaAPI.Models
{
    public class Story
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Author { get; set; }
    }

    public class StoryDBContext : DbContext
    {
        public DbSet<Story> Stories { get; set; }
    }
}