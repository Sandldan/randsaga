﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RandsagaAPI.Models;

namespace RandsagaAPI.Controllers
{
    public class StoriesController : ApiController
    {
        private RandsagaAPIContext db = new RandsagaAPIContext();

        static readonly IStoryRepository repository = new StoryRepository();

        // GET: api/Stories
        public IQueryable<Story> GetStories()
        {
            return db.Stories;
        }

        // GET: api/Stories/5
        [ResponseType(typeof(Story))]
        public async Task<IHttpActionResult> GetStory(int id)
        {
            Story story = await db.Stories.FindAsync(id);
            if (story == null)
            {
                return NotFound();
            }

            return Ok(story);
        }

        // PUT: api/Stories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStory(int id, Story story)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != story.Id)
            {
                return BadRequest();
            }

            db.Entry(story).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Stories
        [ResponseType(typeof(Story))]
        public async Task<IHttpActionResult> PostStory(Story story)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Stories.Add(story);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = story.Id }, story);
        }

        // DELETE: api/Stories/5
        [ResponseType(typeof(Story))]
        public async Task<IHttpActionResult> DeleteStory(int id)
        {
            Story story = await db.Stories.FindAsync(id);
            if (story == null)
            {
                return NotFound();
            }

            db.Stories.Remove(story);
            await db.SaveChangesAsync();

            return Ok(story);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StoryExists(int id)
        {
            return db.Stories.Count(e => e.Id == id) > 0;
        }
    }
}

/*
 ﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using RandsagaAPI.Models;

namespace RandsagaAPI.Controllers
{
    public class StoryController : ApiController
    {
        static readonly IStoryRepository repository = new StoryRepository();

        public Story GetStory(int id)
        {
            Story item = repository.Get(id);
            if (item == null)
            {
                throw  new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }


        public void deleteStory(int id)
        {
            Story story = repository.Get(id);
            if (story == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(id);
        }

        public IEnumerable<Story> GetStories()
        {
            return repository.GetAll();
        }

        public HttpResponseMessage PostStory(Story story)
        {
            story = repository.Add(story);
            var response = Request.CreateResponse<Story>(HttpStatusCode.Created, story);

            string uri = Url.Link("DefaultApi", new {id = story.Id});
            response.Headers.Location = new Uri(uri);

            return response;

        }





        
        public StoryRepository(string name, string category, string author)
        {
            Add(new Story {Name = name, Author = author, Category = category});
        }

        public Story Add(Story item)
        {
            if (item == null)
        }
    }
}

 */